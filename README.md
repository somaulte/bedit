# BEDIT

`bedit` is a **b**ash text **edit**or.

### Dependencies

- `bash`

### Usage

```sh
Usage: bedit {OPTION} [file]
Example: bedit '/etc/issue'

   Options:
   	-h, --help		Display this message.

   Arguments:
   	file			A valid path to a file.

   Environment variables:
   	BEDIT_DEBUG		If set to 'true' or '1', enter debug mode.

```
